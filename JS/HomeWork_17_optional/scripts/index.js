const student = {};

const name = prompt("What is your name?");
const lastName = prompt("What is your surname?");

student.name = name;
student["last name"] = lastName;

student.tabel = {};
while (true) {
    let subject = prompt("Enter subject");
    if (!subject) {
        break;
    }
    student.tabel[subject] = +prompt("Enter grade");
}

let countBadGrade = 0;
let totalCount = 0;
let totalGrade = 0;

for (let key in student.tabel) {
    totalGrade += student.tabel[key];
    totalCount++;

    if (student.tabel[key] < 4) countBadGrade++;
}

if (!countBadGrade) alert("Студент переведен на следующий курс");
if (totalGrade/totalCount > 7) alert("Студенту назначена стипендия");

