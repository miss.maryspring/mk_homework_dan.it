// получение данных от пользователя
let userName = prompt("What is your name?");
let userAge = prompt("How old are you?");

//проверка на корректность введенных данных
while (userName === "" || userName === null) {
    userName = prompt("What is your name?", userName);
}

while (isNaN(userAge) || userAge === "" || userAge === null) {
    userAge = prompt("How old are you?", userAge);
}

//вариант проверки, если пользователь не введет данные, а нажмет отмену (захочет выйти) - доступ будет запрещен
/*if (userName === null || userAge === null) {
        userAge = 1;
} else {
    while (userName === "" || userAge === "" || isNaN(+userAge)) {
        userName = prompt("What is your name?", userName);
        userAge = prompt("How old are you?", userAge);
        if (userName === null || userAge === null) {
            userAge = 1;
            break;
        }
    }
}*/


// основное задание - зависимо от введенного возраста - разрешаем/запрещаем доступ
if (userAge < 18) {
    alert("You are not allowed to visit this website");
} else if (userAge <= 22) {           // или (userAge >= 18 && userAge <= 22)
    confirm("Are you sure you want to continue?")
    ? alert(`Welcome, ${userName}`)
    : alert("You are not allowed to visit this website");
} else {
    alert(`Welcome, ${userName}`);
}