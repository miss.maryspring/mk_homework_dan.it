// Вначале прописываем функции;
// Необязательное задание - проверка введенных чисел (можно сделать просто циклом while);
function validateNum(num) {
    let lastNum = num;
    num = parseFloat(num);

    while (isNaN(num)) {
        num = prompt("Enter number again", lastNum || "");
        lastNum = num;
        num = parseFloat(num);
    }

    return num;
}

function doAction(n1, n2, action) {
    switch (action) {
        case "+":
            return n1 + n2;
        case "-":
            return n1 - n2;
        case "*":
            return n1 * n2;
        case "/":
            return n1 / n2;
        default:
            return alert("Not correct action");
    }
}
let num1 = validateNum(prompt("Enter the first number"));
let num2 = validateNum(prompt("Enter the second number"));
let action = prompt("Enter the action you want to perform");

console.log(doAction(num1, num2, action));
