const passwordInp = document.getElementsByTagName('input')[0];
const confirmPassword = document.getElementsByTagName('input')[1];
const icons = document.getElementsByTagName('i');
const confirmBtn = document.querySelector('.btn');
const errorMessage = document.querySelector('.error');

document.querySelectorAll('input').forEach((elem) => {
    elem.onfocus = () => errorMessage.innerHTML = '';
});

for (let icon of icons) {
    icon.onclick = function() {
        icon.classList.toggle('fa-eye');
        icon.classList.toggle('fa-eye-slash');
        icon.previousElementSibling.setAttribute('type', `${icon.classList.contains('fa-eye') ? 'text' : 'password'}`);
    }
}

confirmBtn.onclick = function () {
    if (passwordInp.value === confirmPassword.value && passwordInp.value !== '') {
        alert('You are welcome');
    } else {
        errorMessage.innerHTML = 'Нужно ввести одинаковые значения';
    }
    passwordInp.value = '';
    confirmPassword.value = '';

    return false;
};