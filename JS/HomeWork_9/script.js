const tabsTitles = document.querySelectorAll('.tabs-title');
const tabsDescriptions = document.querySelectorAll('.tabs-panel');
const container = document.querySelector('div');

container.onclick = function(event) {
    const target = event.target;
    if (target.className === 'tabs-title') {
        document.querySelector('.tabs-title.active').classList.remove('active');
        document.querySelector('.tabs-panel.active').classList.remove('active');

        target.classList.add('active');
        const index = [...tabsTitles].indexOf(target);
        const panel = [...tabsDescriptions].find(el => el.getAttribute('data-index') == index);
        panel.classList.add('active');
    }
};