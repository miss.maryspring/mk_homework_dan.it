function performance(team, tasks, deadline) {
    const readyDate = new Date();
    const efficiency = totalPoints(team);
    const scopeOfTasks = totalPoints(tasks);
    const workDays = Math.ceil(scopeOfTasks / efficiency);
    const workHours = Math.ceil((scopeOfTasks % efficiency) / efficiency * 8);   // кол-во часов (без учета целых дней) для выполнения всех задач
    let overworkHours = 0;
    let dayOfWeek;
debugger;
    readyDate.setHours(0,0,0,0);  // стартуем с полуночи
    deadline.setHours(0);

    for (let i = 0; i < workDays; i++) {
        dayOfWeek = readyDate.getDay();
        if (dayOfWeek === 0 || dayOfWeek === 6) i--;
        readyDate.setDate(readyDate.getDate() + 1);
        if (readyDate > deadline && dayOfWeek !== 0 && dayOfWeek !== 6) overworkHours += 8;
    }

    if (readyDate <= deadline) {
        alert(`Все задачи будут успешно выполнены за ${Math.floor((deadline - readyDate) / 1000 / 3600 / 24)} дней до наступления дедлайна!`)
    } else {
        if (workHours) overworkHours -= (8 - workHours);   // точный подсчет доп. часов
        alert(`Команде разработчиков придется потратить дополнительно ${overworkHours} часов после дедлайна, чтобы выполнить все задачи в беклоге`);
    }
}

function totalPoints(arr) {
    let total = 0;
    for (let value of arr) total += value;
    return total;
}

const arr1 = [20];
const arr2 = [80];
const date1 = new Date("2020-04-26");

performance(arr1, arr2, date1);