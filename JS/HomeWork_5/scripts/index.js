function createNewUser() {
    const firstName = prompt("What is your name?");
    const lastName = prompt("What is your last name?");
    const birthday = prompt("Enter date of birth", "dd.mm.yyyy");

    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        setFirstName(value) {
            Object.defineProperty(this, "firstName", {  //можно сделать как set firstName(value)
                writable: true
            });
            this.firstName = value;
            Object.defineProperty(this, "firstName", {
                writable: false
            });
        },
        setLastName(value) {
            Object.defineProperty(this, "lastName", {
                writable: true
            });
            this.lastName = value;
            Object.defineProperty(this, "lastName", {
                writable: false
            });
        },
        getAge() {
            const now = new Date();
            const currentYear = now.getFullYear();
            const [dateOfBirth, monthOfBirth, yearOfBirth] = this.birthday.split(".");
            let inputBirthday = new Date(+yearOfBirth, monthOfBirth - 1, +dateOfBirth);    // приводим к числу, так как в параметрах ожидаем число, но работает и без унарного плюса
            let age = currentYear - yearOfBirth;                                                   //чтобы не делать доп. вызов inputBirthday.getFullYear();

            return now > inputBirthday.setFullYear(currentYear) ? age : age - 1;
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6, 10);
        }
    };

// запрещаем перезапись свойств объекта;
    Object.defineProperties(newUser, {
        firstName: {
            writable: false,
        },
        lastName: {
            writable: false,
        }
    });

    return newUser;
}
const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());