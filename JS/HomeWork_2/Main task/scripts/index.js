let num = +prompt("Enter a number");

// Необязательное задание - проверка, целое ли число
while (!Number.isInteger(num)) {
    num = +prompt("Enter number, please");
}

if (num > 4) {
    for (let i = 5; i <= num; i += 5) {
        console.log(i);
    }
} else {
    console.log("Sorry, no numbers");
}