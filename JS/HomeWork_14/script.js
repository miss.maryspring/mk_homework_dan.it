$(document).ready(function () {
    $(".submenu").on("click","a", function (event) {
        event.preventDefault();
        const id  = $(this).attr("href"),
            top = $(id).offset().top;
        $("html").animate({scrollTop: top}, 1000);
    });

    const btnToTop = $('.scroll-top');
    const checkScrollingViewPort = (event) => {
        const scrolling = window.scrollY;
        const windowHeight = window.innerHeight;
        if (scrolling > windowHeight) {
            btnToTop.fadeIn(100);
        } else {
            btnToTop.fadeOut(100);
        }
    };

    $(window).scroll(checkScrollingViewPort);

    btnToTop.on('click', () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    });

    const btnSection = $("#toggle");
    btnSection.click(function () {
        $(this).toggleClass('closed');
        const btnText = $(this).hasClass('closed') ? 'Показать' : 'Закрыть';
        $(this).text(btnText);
        btnSection.parent().find("div").slideToggle(100);
    });
});