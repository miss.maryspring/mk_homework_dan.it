function validateNum(num) {
    let lastNum = num;
    num = parseFloat(num);

    while (isNaN(num)) {
        num = prompt("Enter number again", lastNum || "");
        lastNum = num;
        num = parseFloat(num);
    }

    return num;
}

function factorial(num) {
    return (num > 1) ? num * factorial(num - 1) : 1;
}

let num = validateNum(prompt("Enter a number"));
alert(factorial(num));