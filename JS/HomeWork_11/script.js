const buttons = document.querySelectorAll('.btn');

document.onkeydown = function(event) {
    buttons.forEach(btn => {
        btn.style.backgroundColor = `${btn.textContent.toLowerCase() === event.key.toLowerCase() ? 'blue' : ''}`;
    });
};


// второй вариант с добавлением класса

// document.onkeydown = function(event) {
//     buttons.forEach(btn => {
//         if (btn.classList.contains('blue')) btn.classList.remove('blue');
//         if (btn.textContent.toLowerCase() === event.key.toLowerCase()) btn.classList.add('blue');
//     });
// };