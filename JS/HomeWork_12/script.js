const btnRun = document.createElement('button');
btnRun.innerHTML = 'Прекратить';
btnRun.style.marginRight = '180px';

const btnStop = document.createElement('button');
btnStop.innerHTML = 'Возобновить показ';
document.body.append(btnRun, btnStop);

let currentImg = 1;
showBanner(currentImg);

function showBanner(n) {
    const banners = document.querySelectorAll('.image-to-show');
    if (n > banners.length) currentImg = 1;
    if (n < 1) currentImg = banners.length;
    banners.forEach(banner => banner.style.display = "none");
    banners[currentImg - 1].style.display = "block";
}

function nextBanner() {
    showBanner(currentImg += 1);
}

let timerId = setInterval(nextBanner, 2000);

btnRun.onclick = function() {
    clearInterval(timerId);
};
btnStop.onclick = function () {
    timerId = setInterval(nextBanner, 2000);
};






// const timeElem = document.getElementById('timer'),
//     countdown = new Date(),
//     responseTime = new Date(Date.now() + (1000*10)); // таймер 10 секунд
//
// function startTime() {
//     countdown.setTime(responseTime - Date.now());
//     if  (countdown.getSeconds() < 10 ) `0${countdown.getSeconds()}`;
//     timeElem.innerHTML = countdown.getSeconds() + ':' + countdown.getMilliseconds();
//     if(countdown.getSeconds() > 0)
//         requestAnimationFrame(startTime);
// }
// requestAnimationFrame(startTime);
//
