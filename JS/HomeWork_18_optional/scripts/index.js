function deepClone(obj) {
    const clonedObj = {};
    for (const key in obj) {
        if (Array.isArray(obj[key])) {
            //clonedObj[key] = [];
            // for (let i of obj[key]) {
            //     clonedObj[key].push(i);   // сработает только для массива первого уровня
            // }
            clonedObj[key] = JSON.parse(JSON.stringify(obj[key]));   // сработает для вложенных массивов любого уровня
            continue;
        } else if (obj[key] instanceof Object) {
            clonedObj[key] = deepClone(obj[key]);
            continue;
        }
        clonedObj[key] = obj[key];
    }

    return clonedObj;
    
}

const product = {
    title: "apple",
    packing: [500, [1200, 1100, 1000], 2000],
    price: {
        red: {
            small: 10,
            medium: 15,
            big: 20
        },
        green: 15,
        yellow: 30
    }
};

// Проверка
let clonedObj = deepClone(product);
console.log(clonedObj);
product.price.red.medium = 18;
product.packing[0] = 300;
product.packing[1][2] = 1050;
console.log(product);
console.log(clonedObj);