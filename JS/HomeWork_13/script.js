const themeBtn = document.getElementById('btn-theme');
const subscribeBtn = document.querySelector('.subscribe-btn');
const bestArtBtn = document.querySelector('.best-art-btn');
const sections = document.querySelectorAll("[data-style]");

const darkThemeData = {
    sections: "#323232",
    "first-btn": "#1c3443",
    "second-btn": "black"
};
const darkThemeStr = JSON.stringify(darkThemeData);

function changeTheme() {
    if (localStorage.getItem("theme")) {
        localStorage.removeItem("theme");
        subscribeBtn.removeAttribute("style");
        bestArtBtn.removeAttribute("style");
        sections.forEach(item => item.removeAttribute("style"));
    } else {
        subscribeBtn.style.backgroundColor = darkThemeData["first-btn"];
        bestArtBtn.style.backgroundColor = darkThemeData["second-btn"];
        sections.forEach(item => item.style.backgroundColor = darkThemeData.sections);
        localStorage.setItem("theme", darkThemeStr);
    }
}

themeBtn.onclick = changeTheme;

document.body.onload = function () {
    if (localStorage.getItem("theme")) {
        subscribeBtn.style.backgroundColor = darkThemeData["first-btn"];
        bestArtBtn.style.backgroundColor = darkThemeData["second-btn"];
        sections.forEach(item => item.style.backgroundColor = darkThemeData.sections);
    }
};