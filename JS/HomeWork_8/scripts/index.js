const input = document.body.querySelector('.field');

function showMessage(value) {
    const spanEl = document.createElement('span');
    spanEl.textContent = `Current price: ${value} UAN`;
    spanEl.classList.add('info-text');
    const btn = document.createElement('button');
    btn.innerHTML = '&#x2715;';
    btn.classList.add('info-close');
    spanEl.append(btn);
    btn.onclick = closeMessage;

    return spanEl;
}

function closeMessage() {
    this.parentElement.remove();
    input.value = '';
    input.removeAttribute('disabled');
}

function showErrorMessage() {
    const errorPhrase = document.createElement('span');
    errorPhrase.textContent = 'Please enter correct price';
    errorPhrase.classList.add('text-error');

    return errorPhrase;
}

input.onfocus = function() {
    this.classList.add('focused');
    this.classList.remove('text-green');
    if (this.classList.contains('error')) {
        this.classList.remove('error');
        this.nextElementSibling.remove();
    }
};

input.onblur = function() {
    this.classList.remove('focused');
    const inputValue = this.value;
    if (+inputValue >=0 && inputValue !=='') {
        this.classList.add('text-green');
        this.setAttribute('disabled', true);
        document.body.prepend(showMessage(inputValue));
    } else {
        this.classList.add('error');
        this.value = '';
        this.after(showErrorMessage());
    }
};