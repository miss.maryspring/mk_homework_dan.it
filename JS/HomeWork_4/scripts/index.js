function createNewUser() {
    const firstName = prompt("What is your name?");
    const lastName = prompt("What is your last name?");

    const newUser = {
        firstName,
        lastName,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();  // или this.firstName.charAt(0)
        },
        setFirstName(value) {
            Object.defineProperty(this, "firstName", {  //можно сделать как set firstName(value)
                writable: true
            });
            this.firstName = value;
            Object.defineProperty(this, "firstName", {
                writable: false
            });
        },
        setLastName(value) {
            Object.defineProperty(this, "lastName", {
                writable: true
            });
            this.lastName = value;
            Object.defineProperty(this, "lastName", {
                writable: false
            });
        }
    };

// запрещаем перезапись свойств объекта;
    Object.defineProperties(newUser, {
        firstName: {
            writable: false,
        },
        lastName: {
            writable: false,
        }
    });

    return newUser;
}

console.log(createNewUser().getLogin());

// проверка
// const obj = createNewUser();
// obj.lastName = "Petrova";
// obj.setLastName("Bublik");
// obj.lastName = "Honour";
// console.log(obj.getLogin());