function filterBy(arr, dataType) {
    // вариант 1
    if (dataType === "null") return arr.filter( value => value !== null);    //если null как отдельный тип данных
    return arr.filter(value => typeof(value) !== dataType || value === null);

    // вариант 2
    // let newArr = [];
    // for (let value of arr) {
    //     if (dataType === 'null' && value === null) continue;
    //     if (typeof value === dataType && value !== null) continue;
    //     newArr.push(value);
    // }
    // return newArr;
}

let arr = [{'hello': 2}, 'hello', 'world', 23, '23', null, 'null', true];
let type = 'object';

console.log(filterBy(arr, type));
console.log(arr);
