let f0 = +prompt("Enter first num");
let f1 = +prompt("Enter second num");
let n = +prompt("Enter index number n");

function fib(f0, f1, n) {
    if (n > 0) {
        for (let i = 2; i < n; i++) {
            let sum = f0 + f1;
            f0 = f1;
            f1 = sum;
        }
    } else if (n < 0) {
        for (let i = -2; i > n; i--) {
            let sub = f0 - f1;
            f0 = f1;
            f1 = sub;
        }
    }

    return f1;
}

alert(fib(f0, f1, n));
