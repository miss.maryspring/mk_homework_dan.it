function createLiElem(elemArr) {
    const resultArr = elemArr.map(elem => {
        return Array.isArray(elem) ? `<li><ul>${createLiElem(elem).join('')}</ul></li>` : `<li>${elem}</li>`;
    });

    return resultArr;
}

function  createList(arr) {
    const list = document.createElement('ul');
    const mappedArr = createLiElem(arr);
    mappedArr.forEach(li => list.insertAdjacentHTML('beforeend', li));
    document.body.prepend(list);
}

let seconds = document.getElementById('countdown').textContent;
let countdown = setInterval(() => {
    seconds--;
    document.getElementById('countdown').textContent = seconds;
    if (seconds <= 0) {
        document.body.innerHTML = '';
        clearInterval(countdown);
    }
}, 1000);

const testArr = [5, ['Kiev', 'Chernihiv', ['hello', [1, 2, 3], 'world']], 'Kharkiv', 'Odessa', 'Lviv'];
createList(testArr);