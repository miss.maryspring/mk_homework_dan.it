const {src, dest, watch, task, series, parallel} = require('gulp'),
      sass = require('gulp-sass'),
      concat = require('gulp-concat'),
      prefixer = require('gulp-autoprefixer'),
      // purgeCss = require('gulp-purgecss'),
      minifyCss = require('gulp-clean-css'),
      rename = require('gulp-rename'),
      uglify = require('gulp-uglify-es').default,
      minifyImg = require('gulp-imagemin'),
      remove = require('gulp-clean'),
      sourcemaps = require('gulp-sourcemaps'),
      browserSync = require('browser-sync').create();

// коллбэки для тасков

const cssAll = () => {
    return src('./src/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(concat('styles.css'))
        .pipe(prefixer({overrideBrowserslist: ['last 10 versions']}))
        // .pipe(purgeCss({         удаляет неиспользуемый css (относительно html - классы, теги)
        //     content: ['index.html']
        // }))
        .pipe(minifyCss({
            format: 'beautify',    // стандартный вид css-правил
            level: {
                1: {specialComments: 0},     //удаляет комментарии /* , избегаем доп. сжатия
                2: {removeDuplicateRules: true}
            }
        }))
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('./map'))
        .pipe(dest('dist/css'))
        .pipe(browserSync.stream());
}

const jsAll = () => {
    return src('./src/js/*.js')
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(dest('dist/js'))
        .pipe(browserSync.stream());
}

const imgAll = () => {
    return src('./src/img/*.png')
        .pipe(minifyImg())
        .pipe(dest('dist/img'))
        .pipe(browserSync.stream());
}

const clearDir = () => {
    return src('./dist', {read: false, allowEmpty: true})
        .pipe(remove());
}

const filesWatcher = () => {
    watch('src/scss/**/*.scss', cssAll);
    watch('./src/js/*.js', jsAll);
    watch('./src/img/*.png', jsAll);
}

const server = () => {
    browserSync.init({
        server: {
            baseDir: "./",
        }
    });
    watch('./*.html').on('change', browserSync.reload);
}


//таски

task('build', series(clearDir, cssAll, jsAll, imgAll));
task('dev', parallel(filesWatcher, server));
task('default', series('build', 'dev'));
