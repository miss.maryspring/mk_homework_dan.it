document.addEventListener('DOMContentLoaded', function () {
    const menuBtn = document.getElementById('menu-burger');
    const menuBtnIcon = document.querySelector('i');
    const dropMenu = document.querySelector('.main-menu__list');
    let openMenu = false;

    function toggleClass(elem, firstClass, secondClass) {
        elem.classList.toggle(firstClass);
        elem.classList.toggle(secondClass);
    };

    menuBtn.onclick = () => {
        toggleClass(menuBtnIcon, 'fa-bars', 'fa-times');
        dropMenu.style.display = menuBtnIcon.classList.contains('fa-bars') ? 'none' : 'flex';
    };

    window.onresize = function () {
        if (window.innerWidth >= 768 && !openMenu) {
            openMenu = true;
            dropMenu.style.display = 'flex';
            menuBtnIcon.classList.contains('fa-bars') && toggleClass(menuBtnIcon, 'fa-bars', 'fa-times');
        }
        if (window.innerWidth < 768 && openMenu) {
            openMenu = false;
            dropMenu.style.display = 'none';
            menuBtnIcon.classList.contains('fa-times') && toggleClass(menuBtnIcon, 'fa-bars', 'fa-times');
        }
    }

    dropMenu.onclick = (event) => {
        document.querySelectorAll('.main-menu__item').forEach((elem) => {
            elem.classList.remove('active');
        });
        event.target.classList.add('active');
    };

});