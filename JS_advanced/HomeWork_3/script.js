class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }
    get age() {
        return this._age;
    }
    get salary() {
        return this._salary;
    }
    set name(value) {
        this._name = value;
    }
    set age(value) {
        this._age = value;
    }
    set salary(value) {
        this._salary = value;
    }
}


class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salary() {
        return this._salary * 3;
    }
    set salary(value) {
        this._salary = value;
    }
}


const worker1 = new Programmer('Maksim', 27, 1000, ['Java', 'Python']);
const worker2 = new Programmer('Mary', 30, 2200, ['JS', 'Ruby', 'TypeScript']);
const worker3 = new Programmer('Ivan', 25, 2000, ['C', 'PhP']);
console.log(worker1);
console.log(worker2);
console.log(worker3);
// worker3.salary = 500;
// console.log(worker3.salary);