const btn = document.querySelector('button');
const urlForIp = 'https://api.ipify.org/?format=json';
const API = `http://ip-api.com/json`;
const fields = 'continent,country,regionName,city,district';

const showData = (data) => {
    const divEl = document.createElement('div');
    const fragment = new DocumentFragment();

    for (const prop in data) {
        const pElem = document.createElement('p');
        pElem.innerText = `${prop}: ${data[prop] || 'no data available'}`;
        fragment.append(pElem);
    }

    divEl.append(fragment);
    document.body.append(divEl);
}

const getIp = async (url) => {
    const response = await fetch(url);
    const {ip} = await response.json();

    return getFullAddress(ip);

}

const getFullAddress = async (ip) => {
    const response = await fetch(`${API}/${ip}?fields=${fields}`);
    const data = await response.json();
    return showData(data);
};

btn.onclick = () => getIp(urlForIp);

