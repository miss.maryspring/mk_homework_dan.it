const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const divEl = document.createElement('div');
divEl.setAttribute('id', 'root');
document.body.append(divEl);

const list = document.createElement('ul');
divEl.append(list);

function createList(items) {
    const fragment = new DocumentFragment();
    items.forEach(({author, name, price}, i) => {
        try {
            if (!author) throw new Error('неполные данные: отсутствует автор');
            if (!name) throw new Error('неполные данные: отсутствует название');
            if (!price) throw new Error('неполные данные: отсутствует цена');

            const li = document.createElement('li');
            li.innerHTML = `Автор книги: ${author}, название: ${name}, цена: ${price} грн.`
            fragment.append(li);
        } catch (e) {
            console.error(`В книге №${++i} ${e.message}`, {author, name, price});
        }
        list.append(fragment);
    });
}

createList(books);