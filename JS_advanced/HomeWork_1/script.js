const cities = ["Kyiv", "Berlin", "Dubai", "Moscow", "Paris"];

// task 1
const [kyivCity, berlinCity, dubaiCity, moscowCity, parisCity] = cities;
console.log(kyivCity, berlinCity, dubaiCity, moscowCity, parisCity);


// task 2
const employee = {
    name: 'Mary',
    salary: 2000
};

const {name, salary} = employee;
console.log(name, salary);

// task 3
const array = ['value', 'showValue'];

const [value, showValue] = array;
alert(value);
alert(showValue);