const btn = document.querySelector('button');
const urlForIp = 'https://api.ipify.org/?format=json';
const API = `http://ip-api.com/json`;
const fields = 'continent,country,regionName,city,district';

const showData = (data) => {
    const divEl = document.createElement('div');
    const fragment = new DocumentFragment();

    for (const prop in data) {
        const pElem = document.createElement('p');
        pElem.innerText = `${prop}: ${data[prop] || 'no data available'}`;
        fragment.append(pElem);
    }

    divEl.append(fragment);
    document.body.append(divEl);
}

const getIp = (url) => {
    return fetch(url)
        .then(response => response.json())
        .then(({ip}) => getFullAddress(ip));
}

const getFullAddress = (ip) => {
    fetch(`${API}/${ip}?fields=${fields}`)
        .then(response => response.json())
        .then(data => showData(data));
};

btn.onclick = () => getIp(urlForIp);

// const API = `http://ip-api.com/json/${getIp(urlForIp)}?fields=continent,country,regionName,city,district`;

