import React, { Component } from 'react';
import './App.scss';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends Component {
    state = {
        isCreateModalOpen: false,
        isDeleteModalOpen: false
    }

    render() {
        return (
            <>
                {this.state.isCreateModalOpen && (
                    <Modal
                        header='Do you want to create new file?'
                        closeButton={false}
                        text='Check the correctness of the entered data. Are you sure you want to create it?'
                        actions={[
                            <Button text='Create' className='btn-modal-actions' backgroundColor='#518054' onClick={this.closeModal} key='btn1'/>,
                            <Button text='Cancel' className='btn-modal-actions' backgroundColor='#518054' onClick={this.closeModal} key='btn2'/>
                        ]}
                        closeModal={this.closeModal}
                        modifier='create'
                    />
                )}
                {this.state.isDeleteModalOpen && (
                    <Modal
                        header='Do you want to delete this file?'
                        closeButton={true}
                        text='Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'
                        actions={[
                            <Button text='Ok' className='btn-modal-actions btn-modal-actions--delete' backgroundColor='#B3382C' onClick={this.closeModal} key='btn1'/>,
                            <Button text='Cancel' className='btn-modal-actions btn-modal-actions--delete' backgroundColor='#B3382C' onClick={this.closeModal} key='btn2'/>
                        ]}
                        closeModal={this.closeModal}
                        modifier='delete'
                    />
                )}
                <div className="btns-container">
                    <Button text='Open create modal' className='btn-modal-open' backgroundColor='#20BCF2' onClick={this.openCreateFileModal}/>
                    <Button text='Open delete modal' className='btn-modal-open' backgroundColor='#FEEF0C' onClick={this.openDeleteFileModal}/>
                </div>
            </>
        );
    }

    openCreateFileModal = () => this.setState({isCreateModalOpen: true, isDeleteModalOpen: false});

    openDeleteFileModal = () => this.setState({isCreateModalOpen: false, isDeleteModalOpen: true});

    closeModal = () => this.setState({isCreateModalOpen: false, isDeleteModalOpen: false});
}

export default App;
