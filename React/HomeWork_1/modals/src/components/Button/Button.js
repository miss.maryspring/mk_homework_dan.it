import React, { PureComponent } from 'react';

class Button extends PureComponent {
    render() {
        const { text, backgroundColor, onClick, className } = this.props;
        return (
            <button className={className} style={{backgroundColor: backgroundColor}} onClick={onClick}>
                {text}
            </button>
        );
    }
}

export default Button;