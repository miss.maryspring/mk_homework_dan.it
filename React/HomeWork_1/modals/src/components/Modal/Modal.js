import React, { PureComponent } from 'react';
import Button from "../Button/Button";

class Modal extends PureComponent {

    render() {
        const { header, closeButton, text, actions, closeModal, modifier } = this.props;

        return (
            <>
                <div className={"modal modal--" + modifier}>
                    <div className={"modal-header modal-header--" + modifier}>
                        <span>{header}</span>
                        {closeButton && <Button text='&#x2715;' className="btn-close-icon" backgroundColor='transparent' onClick={closeModal} />}
                    </div>
                    <div className="modal-body">
                        <p>{text}</p>
                    </div>
                    <div className="modal-footer">
                        {actions}
                    </div>
                </div>
                <div className="backdrop" onClick={closeModal}></div>
            </>
        );
    }
}

export default Modal;