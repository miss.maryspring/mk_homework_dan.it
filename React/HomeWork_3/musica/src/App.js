import React, { useEffect, useState } from 'react';
import './App.scss';
import axios from 'axios';
import LocalStorage from './localStorage';
import UpperHeader from './components/UpperHeader/UpperHeader';
import Menu from './components/Menu/Menu';
import Footer from './components/Footer/Footer';
import LowerFooter from './components/LowerFooter/LowerFooter';
import AppRoutes from './routes/AppRoutes';

const App = (props) => {
    const [products, setProducts] = useState([]);
    const [productsInCart, setProductsInCart] = useState(JSON.parse(LocalStorage.getCartList()) || []);
    const [favourites, setFavourites] = useState(JSON.parse(LocalStorage.getFavouritesList()) || []);
    const [isSidenavOpen, setIsSidenavOpen] = useState(false);

    useEffect(() => {
        axios.get('./products.json')
            .then(resp => setProducts(resp.data))
            .catch(console.error);
    }, []);

    const toggleFavourites = (...params) => {
        LocalStorage.setFavouritesList(...params);
        setFavourites(JSON.parse(LocalStorage.getFavouritesList()));
    }

    const deleteFromCart = (id) => {
        LocalStorage.deleteFromCart(id);
        setProductsInCart(JSON.parse(LocalStorage.getCartList()));
    }

    const addToCart = (...params) => {
        LocalStorage.setCartList(...params);
        setProductsInCart(JSON.parse(LocalStorage.getCartList()));
    }

    return (
        <>
            <UpperHeader
                productsInCart={productsInCart}
                deleteItem={deleteFromCart}
            />
            <Menu
                isSidenavOpen={isSidenavOpen}
                toggleNav={() => setIsSidenavOpen(!isSidenavOpen)}
            />
            <AppRoutes
                products={products}
                favourites={favourites}
                productsInCart={productsInCart}
                addToCart={addToCart}
                deleteFromCart={deleteFromCart}
                toggleFavourites={toggleFavourites}
            />
            <Footer/>
            <LowerFooter/>
        </>
    )
}

export default App;