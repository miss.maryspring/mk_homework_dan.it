import React from 'react';
import { Route, Switch } from 'react-router-dom';
import HomePage from '../pages/Home/HomePage';
import Favourites from '../pages/Favourites/Favourites';
import CartPage from '../pages/Cart/CartPage';
import PropTypes from 'prop-types';

const AppRoutes = (props) => {
    const { products, favourites, productsInCart, addToCart, toggleFavourites, deleteFromCart } = props;

    return (
        <Switch>
            <Route exact path='/' render={() => <HomePage
                products={products}
                favourites={favourites}
                productsInCart={productsInCart}
                addToCart={addToCart}
                toggleFavourites={toggleFavourites}
            />} />
            <Route path='/favourites' render={() => <Favourites
                favourites={favourites}
                toggleFavourites={toggleFavourites}
                addToCart={addToCart}
            />} />
            <Route path='/cart' render={() => <CartPage
                items={productsInCart}
                deleteItem={deleteFromCart}
            />} />
        </Switch>
    );
}

AppRoutes.propTypes = {
    products: PropTypes.array,
    favourites: PropTypes.array,
    productsInCart: PropTypes.array,
    addToCart: PropTypes.func.isRequired,
    toggleFavourites: PropTypes.func.isRequired,
    deleteFromCart: PropTypes.func.isRequired
}

AppRoutes.defaultProps = {
    products: [],
    favourites: [],
    productsInCart: [],
}

export default AppRoutes;