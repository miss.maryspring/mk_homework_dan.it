import React, { useState } from 'react';
import Button from '../Button/Button';
import '../../localStorage';
import Modal from '../Modal/Modal';
import PropTypes from 'prop-types';
import Rating from '../Rating/Rating';

const Card = (props) => {
    const [isModal, setIsModal] = useState(false);
    const { src, title, artist, desc, price, discount, rating, isFavourite, addToCart, toggleFavourites } = props;

    return (
        <>
            {isModal &&
                <Modal
                    header='Confirm your selection'
                    closeButton={true}
                    text='Check the correctness of the entered data. Are you sure you want to add this item to the cart?'
                    actions={[
                        <Button children='Add' className='btn-modal-actions' onClick={() => addToCart(props)} key='btn1'/>,
                        <Button children='Cancel' className='btn-modal-actions' onClick={() => setIsModal(false)} key='btn2'/>
                    ]}
                    closeModal={() => setIsModal(false)}
                />
            }
            <div className='products__card'>
                <div className='products__image-block'>
                    <img className='products__image' src={src} alt='product' />
                    <Button
                        className={
                            isFavourite
                                ? 'products__favourite products__favourite--active'
                                : 'products__favourite'
                        }
                        onClick={() => toggleFavourites(props)}
                    >{<i className="fas fa-star" />}</Button>
                    {discount && (<div className='products__sale'>Sale</div>)}
                </div>
                <div className='products__content'>
                   <h4 className='products__title'>
                       {title}<br />
                       <span>by {artist}</span>
                   </h4>
                    <Rating rating={rating} />
                    <p className='products__info'>{desc}</p>
                    <div className='products__buy buy'>
                        <div className='buy__price price'>
                            {discount &&
                                (
                                    <>
                                        <span className='price__old'><s>${price}</s></span>
                                        <span className='price__new'>${discount}</span>
                                    </>
                                )
                            }
                            {!discount && (<span>${price}</span>)}
                        </div>
                        <Button className='buy__btn' children='Add to cart' onClick={() => setIsModal(true)}/>
                    </div>
               </div>
            </div>
        </>
    );
}

Card.propTypes = {
    src: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    artist: PropTypes.string,
    desc: PropTypes.string,
    price: PropTypes.string.isRequired,
    discount: PropTypes.string,
    rating: PropTypes.number,
    addToCart: PropTypes.func.isRequired,
    isFavourite: PropTypes.bool.isRequired,
    toggleFavourites: PropTypes.func.isRequired
}

Card.defaultProps = {
    artist: 'Unknown',
    desc: 'Information in progress',
    discount: null,
    rating: 0
}

export default Card;