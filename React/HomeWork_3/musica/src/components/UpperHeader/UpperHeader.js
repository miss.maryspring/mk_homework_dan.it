import React, { useState } from 'react';
import Button from '../Button/Button';
import Cart from '../Cart/Cart';
import PropTypes from 'prop-types';

const UpperHeader = (props) => {
    const [isCartOpen, setIsCartOpen] = useState(false);
    const { productsInCart, deleteItem } = props;

    return (
        <header className='upper-header'>
            <div className='upper-header__upper-line' />
            <div className="upper-header__content">
                <div className='container'>
                    <div className='upper-header__socials'>
                        <a href='#' className='upper-header__social-icon upper-header__social-icon--fb'>
                            <i className='fab fa-facebook-f' />
                        </a>
                        <a href='#' className='upper-header__social-icon upper-header__social-icon--drib'>
                            <i className='fab fa-dribbble' />
                        </a>
                        <a href='#' className='upper-header__social-icon upper-header__social-icon--tw'>
                            <i className='fab fa-twitter' />
                        </a>
                        <a href='#' className='upper-header__social-icon upper-header__social-icon--mail'>
                            <i className='far fa-envelope' />
                        </a>
                        <a href='#' className='upper-header__social-icon upper-header__social-icon--vimeo'>
                            <i className='fab fa-vimeo-v' />
                        </a>
                    </div>
                    <div className='upper-header__administration'>
                        <a href='#' className='upper-header__login-link'>Login</a> /
                        <a href='#' className='upper-header__register-link'> Register</a>
                        <Button
                            onClick={() => setIsCartOpen(!isCartOpen)}
                            className='upper-header__cart'
                            children={<><i className='fas fa-cart-arrow-down' /><span>Cart</span></>}
                        />
                        {isCartOpen && <Cart productsInCart={productsInCart} deleteItem={deleteItem} closeCart={() => setIsCartOpen(!isCartOpen)} />}
                    </div>
                </div>
            </div>
        </header>
    );
}

UpperHeader.propTypes = {
    productsInCart: PropTypes.array,
    deleteItem: PropTypes.func.isRequired
}

UpperHeader.defaultProps = {
    productsInCart: []
}

export default UpperHeader;