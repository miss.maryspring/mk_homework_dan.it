import React from 'react';
import Card from '../Card/Card';
import PropTypes from 'prop-types';

const Products = (props) => {
    const { products, favourites, addToCart, toggleFavourites } = props;

    return (
        <div className='products'>
            {
                products.map(item =>
                    (
                        <Card
                            key={item.id}
                            id={item.id}
                            src={item.path}
                            title={item.title}
                            artist={item.artist}
                            rating={item.rating}
                            desc={item.desc}
                            price={item.price}
                            discount={item.discount}
                            addToCart={addToCart}
                            isFavourite={!!favourites.find(favourProd => item.id === favourProd.id)}
                            toggleFavourites={toggleFavourites}
                        />
                    )
                )
            }
        </div>
    );
}

Products.propTypes = {
    products: PropTypes.array,
    favourites: PropTypes.array,
    addToCart: PropTypes.func.isRequired,
    toggleFavourites: PropTypes.func.isRequired
}

Products.defaultProps = {
    products: [],
    favourites: []
}

export default Products;