import React, { useState } from 'react';
import Rating from '../Rating/Rating';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';
import PropTypes from 'prop-types';

const CartCard = (props) => {
    const [isModal,setIsModal] = useState(false);
    const { id, title, artist, src, rating, price, total, deleteItem } = props;

    return (
        <>
            {isModal &&
            <Modal
                header='Confirm deleting'
                closeButton={true}
                text='Are you sure you want to delete this item from the cart?'
                actions={[
                    <Button children='Delete' className='btn-modal-actions' onClick={() => deleteItem(id)} key='btn1'/>,
                    <Button children='Cancel' className='btn-modal-actions' onClick={() => setIsModal(false)} key='btn2'/>
                ]}
                closeModal={() => setIsModal(false)}
            />
            }
            <div>
                <div className='openCart__item cart-item'>
                    <div className='cart-item__content'>
                        <img src={src} alt='' />
                        <div className='cart-item__info'>
                            <h4 className='cart-item__title'>{total} x {title} &nbsp;<span>by {artist}</span></h4>
                            <Rating rating={rating}/>
                        </div>
                    </div>
                    <div className='cart-item__price-block'>
                        <div className='cart-item__price'>${(price * total).toFixed(2)}</div>
                        <Button onClick={() => setIsModal(true)} children='X' className='cart-item__delete-btn'/>
                    </div>
                </div>
            </div>
        </>
    );
}

CartCard.propTypes = {
    id: PropTypes.string.isRequired,
    src: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    artist: PropTypes.string,
    price: PropTypes.string.isRequired,
    rating: PropTypes.number,
    deleteItem: PropTypes.func.isRequired,
    total: PropTypes.number.isRequired
}

CartCard.defaultProps = {
    artist: 'Unknown',
    rating: null
}

export default CartCard;