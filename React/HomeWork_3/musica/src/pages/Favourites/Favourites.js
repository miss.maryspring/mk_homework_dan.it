import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Card from '../../components/Card/Card';

const Favourites = (props) => {
    const { favourites, toggleFavourites, addToCart } = props;

    return (
        <div className='favourites-page'>
            <div className="container">
                <h2 className='favourites-page__title'>Your list of favourites</h2>
                <div className='favourites-page__content'>
                    {!favourites.length && (<p className='default-message'>No favourite items...</p>)}
                    {!!favourites.length && (
                        favourites.map(item => (
                                    <Card
                                        key={item.id}
                                        id={item.id}
                                        src={item.src}
                                        title={item.title}
                                        artist={item.artist}
                                        rating={item.rating}
                                        desc={item.desc}
                                        price={item.price}
                                        discount={item.discount}
                                        addToCart={addToCart}
                                        isFavourite={!!favourites.find(favourProd => item.id === favourProd.id)}
                                        toggleFavourites={toggleFavourites}
                                    />
                                )
                            )
                        )
                    }
                </div>
                <div className='cart-page__controls'>
                    <Link to='/' className='cart-page__btn goBack-btn'>Go back</Link>
                </div>
            </div>
        </div>
    );
}

Favourites.propTypes = {
    favourites: PropTypes.array,
    toggleFavourites: PropTypes.func.isRequired
}

Favourites.defaultTypes = {
    favourites: []
}

export default Favourites;