import React from 'react';
import MainPoster from '../../components/MainPoster/MainPoster';
import MainPage from '../../components/MainPage/MainPage';
import PropTypes from 'prop-types';

const HomePage = (props) => {
    const { products, favourites, toggleFavourites, productsInCart, addToCart } = props;

    return (
        <>
            <MainPoster/>
            <MainPage
                products={products}
                favourites={favourites}
                toggleFavourites={toggleFavourites}
                productsInCart={productsInCart}
                addToCart={addToCart}
            />
        </>
    );
}

HomePage.propTypes = {
    products: PropTypes.array,
    favourites: PropTypes.array,
    toggleFavourites: PropTypes.func.isRequired,
    productsInCart: PropTypes.array,
    addToCart: PropTypes.func.isRequired
}

HomePage.defaultTypes = {
    products: [],
    favourites: [],
    productsInCart: []
}

export default HomePage;