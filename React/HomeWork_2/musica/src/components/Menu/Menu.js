import React, { PureComponent } from 'react';

class Menu extends PureComponent {
    render() {
        return (
            <menu className='main-menu'>
                <div className='container'>
                    <a href='#' className='main-menu__logo'>
                        <img className='main-menu__logo-pic' src='./img/m-logo.png' alt='logo' />
                        <span className='main-menu__logo-title'>Store</span>
                    </a>
                    <nav className='main-menu__nav main-nav'>
                        <ul className='main-nav__list'>
                            <li className='main-nav__item'><a href="#" className='main-nav__link'>Home</a></li>
                            <li className='main-nav__item'><a href="#" className='main-nav__link'>CD's</a></li>
                            <li className='main-nav__item'><a href="#" className='main-nav__link'>DVD's</a></li>
                            <li className='main-nav__item'><a href="#" className='main-nav__link'>News</a></li>
                            <li className='main-nav__item'><a href="#" className='main-nav__link'>Portfolio</a></li>
                            <li className='main-nav__item'><a href="#" className='main-nav__link'>Contact us</a></li>
                        </ul>
                    </nav>
                </div>
            </menu>
        );
    }
}

export default Menu;