import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class Button extends PureComponent {
    render() {
        const { children, onClick, className } = this.props;
        return (
            <button className={className} onClick={onClick}>
                {children}
            </button>
        );
    }
}

Button.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func.isRequired,
    className: PropTypes.string.isRequired
}

Button.defaultProps = {
    text: 'Click'
}

export default Button;