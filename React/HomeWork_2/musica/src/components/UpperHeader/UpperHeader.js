import React, { PureComponent } from 'react';
import Button from '../Button/Button';
import Cart from '../Cart/Cart';
import PropTypes from 'prop-types';
import LocalStorage from "../../localStorage";

class UpperHeader extends PureComponent {
    state = {
        isCartOpen: false
    }

    render() {
        const { productsInCart, deleteItem } = this.props;

        return (
            <header className='upper-header'>
                <div className='upper-header__upper-line' />
                <div className="upper-header__content">
                    <div className='container'>
                        <div className='upper-header__socials'>
                            <a href="#" className='upper-header__social-icon upper-header__social-icon--fb'>
                                <i className='fab fa-facebook-f' />
                            </a>
                            <a href="#" className='upper-header__social-icon upper-header__social-icon--drib'>
                                <i className='fab fa-dribbble' />
                            </a>
                            <a href="#" className='upper-header__social-icon upper-header__social-icon--tw'>
                                <i className='fab fa-twitter' />
                            </a>
                            <a href="#" className='upper-header__social-icon upper-header__social-icon--mail'>
                                <i className='far fa-envelope' />
                            </a>
                            <a href="#" className='upper-header__social-icon upper-header__social-icon--vimeo'>
                                <i className='fab fa-vimeo-v' />
                            </a>
                        </div>
                        <div className='upper-header__administration'>
                            <a href="#" className='upper-header__login-link'>Login</a> /
                            <a href="#" className='upper-header__register-link'> Register</a>
                            <Button
                                onClick={this.toggleCart}
                                className='upper-header__cart'
                                children={<><i className='fas fa-cart-arrow-down' /><span>Cart</span></>}
                            />
                            {this.state.isCartOpen && <Cart productsInCart={productsInCart} deleteItem={deleteItem} />}
                        </div>
                    </div>
                </div>
            </header>
        );
    }

    toggleCart = () => this.setState({isCartOpen: !this.state.isCartOpen});
}

UpperHeader.propTypes = {
    isCartOpen: PropTypes.bool.isRequired
}

export default UpperHeader;