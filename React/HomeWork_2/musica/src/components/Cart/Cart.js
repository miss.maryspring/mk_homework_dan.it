import React, { PureComponent } from 'react';
import LocalStorage from '../../localStorage';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import Rating from '../Rating/Rating';

class Cart extends PureComponent {
    render() {
        const { productsInCart, deleteItem } = this.props;

        return (
            <div className='openCart'>
                {productsInCart.map(item => (
                    <div key={item.id} className='openCart__item cart-item'>
                        <div className='cart-item__content'>
                            <img src={item.path} alt='image' />
                            <div className='cart-item__info'>
                                <h4 className='cart-item__title'>{item.total} x {item.title} &nbsp;<span>by {item.artist}</span></h4>
                                <Rating rating={item.rating}/>
                            </div>
                        </div>
                        <div className='cart-item__price-block'>
                            <div className='cart-item__price'>${(item.price * item.total).toFixed(2)}</div>
                            <Button onClick={()=>deleteItem(item.id)} children='X' className='cart-item__delete-btn'/>
                        </div>
                    </div>
                ))}
                <div className='openCart__total'>
                    <h4 className='openCart__total-subtitle'>Total delivery cost:</h4>
                    <div className='openCart__total-sum'>
                        ${productsInCart.reduce( (prev, item) => prev + item.price * item.total, 0).toFixed(2)}
                    </div>
                </div>
                <div className='openCart__controls'>
                    <Button onClick={this.viewCart} className='openCart__btns viewCart-btn' children='View Cart &nbsp; &#x2192;'/>
                    <Button onClick={this.checkout} className='openCart__btns checkout-btn' children='Proceed to Checkout &nbsp; &#x2192;' />
                </div>
            </div>
        );
    }

    viewCart = () => {}
    checkout = () => {}
}

Cart.propTypes = {
    productsInCart: PropTypes.array
}

Cart.defaultProps = {
    productsInCart: []
}

export default Cart;