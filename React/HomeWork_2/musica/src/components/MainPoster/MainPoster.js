import React, { PureComponent } from 'react';

class MainPoster extends PureComponent {
    render() {
        return (
            <div className='main-poster'>
                <div className='main-poster__img'>
                    <div className='main-poster__subtitle'>
                        <span>Music event with DJ starting at 20.00 on August 15th</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default MainPoster;