import React, { PureComponent } from 'react';

class Rating extends PureComponent {
    render() {
        return (
            <div className='products__rating'>
                {this.setRating()}
            </div>
        );
    }

    setRating = () => {
        const stars = this.props.rating;
        let ratingFragment = [];

        for (let i = 1; i <= 5; i++) {
            i <= stars
                ? ratingFragment.push(<i key={i} className="fas fa-star fa-star--active" />)
                : ratingFragment.push(<i key={i} className="fas fa-star" />)
        }

        return ratingFragment;
    }
}

export default Rating;