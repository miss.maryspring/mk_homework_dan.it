import React, { PureComponent } from 'react';
import Card from "../Card/Card";
import PropTypes from 'prop-types';

class Products extends PureComponent {
    render() {
        const { products, favourites, addToCart, addToFavour } = this.props;

        return (
            <div className='products'>
                {
                    products.map(item =>
                        (
                            <Card
                                key={item.id}
                                src={item.path}
                                title={item.title}
                                artist={item.artist}
                                rating={item.rating}
                                desc={item.desc}
                                price={item.price}
                                discount={item.discount}
                                id={item.id}
                                addToCart={addToCart}
                                isFavourite={!!favourites.find(favourProd => item.id === favourProd.id)}
                                addToFavour={addToFavour}
                            />
                        )
                    )
                }
            </div>
        );
    }
}

Products.propTypes = {
    products: PropTypes.array,
    favourites: PropTypes.array,
    addToCart: PropTypes.func.isRequired,
    addToFavour: PropTypes.func.isRequired
}

Products.defaultProps = {
    products: [],
    favourites: []
}

export default Products;