import React, { Component } from 'react';
import './App.scss';
import axios from 'axios';
import LocalStorage from './localStorage';
import UpperHeader from './components/UpperHeader/UpperHeader';
import Menu from './components/Menu/Menu';
import MainPoster from './components/MainPoster/MainPoster';
import MainPage from './components/MainPage/MainPage';
import Footer from "./components/Footer/Footer";
import LowerFooter from './components/LowerFooter/LowerFooter';


class App extends Component {
    state = {
        products: [],
        productsInCart: JSON.parse(LocalStorage.getCartList()) || [],
        favouriteProducts: JSON.parse(LocalStorage.getFavouritesList()) || []
    }

    render() {
        const { products, productsInCart, favouriteProducts } = this.state;

        return (
            <>
                <UpperHeader
                    productsInCart={productsInCart}
                    deleteItem={this.deleteItem}
                />
                <Menu/>
                <MainPoster/>
                <MainPage
                    products={products}
                    productsInCart={productsInCart}
                    favourites={favouriteProducts}
                    addToCart={this.addToCart}
                    addToFavour={this.addToFavour}
                />
                <Footer />
                <LowerFooter />
            </>
        );
    }

    componentDidMount() {
        axios.get('./products.json')
            .then(resp => this.setState({products: resp.data}))
            .catch(console.error);
    }

    deleteItem = (id) => {
        LocalStorage.deleteItem(id);
        this.setState({productsInCart: JSON.parse(LocalStorage.getCartList())})
    }

    addToCart = (...params) => {
        LocalStorage.setCartList(...params);
        this.setState({productsInCart: JSON.parse(LocalStorage.getCartList())})
    }

    addToFavour = (id, title) => {
        LocalStorage.setFavouritesList(id, title);
        this.setState({favouriteProducts: JSON.parse(LocalStorage.getFavouritesList())})
    }
}

export default App;