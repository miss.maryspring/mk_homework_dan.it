class LocalStorage {
    static getFavouritesList() {
        return localStorage.getItem('favouriteProducts');
    }

    static getCartList() {
        return localStorage.getItem('inCart');
    }

    static setFavouritesList(id, title) {
        const products = LocalStorage.getFavouritesList() || [];

        if (!products.length) {
            const product = {
                id,
                title
            }
            localStorage.setItem('favouriteProducts', JSON.stringify([product]));
        } else {
            const savedProducts = JSON.parse(products);
            const savedProductIndex = savedProducts.findIndex(item => item.id === id);

            if (savedProductIndex !== -1) {
                savedProducts.splice(savedProductIndex, 1);
            } else {
                savedProducts.push({id, title});
            }
            localStorage.setItem('favouriteProducts', JSON.stringify(savedProducts))
        }
    }

    static setCartList(id, title, artist, rating, discount, price, path) {
        const products = LocalStorage.getCartList() || [];

        if (!products.length) {
            const product = {id, title, artist, rating, price: discount || price, path, total: 1}
            localStorage.setItem('inCart', JSON.stringify([product]));
        } else {
            const savedProducts = JSON.parse(products);
            const savedProduct = savedProducts.find(item => item.id === id);

            if (savedProduct) {
                savedProduct.total++;
            } else {
                savedProducts.push({id, title, artist, rating, price: discount || price, path, total: 1});
            }
            localStorage.setItem('inCart', JSON.stringify(savedProducts))
        }
    }

    static deleteItem(id) {
        const cartProducts = JSON.parse(LocalStorage.getCartList());
        const targetItemIndex = cartProducts.findIndex(item => item.id === id);
        if (targetItemIndex !== -1) {
            cartProducts.splice(targetItemIndex, 1);
            localStorage.setItem('inCart', JSON.stringify(cartProducts));
        }
    }
}

export default LocalStorage;